FROM node:lts-alpine3.19@sha256:291e84d956f1aff38454bbd3da38941461ad569a185c20aa289f71f37ea08e23

COPY package.json /opt/app/package.json

WORKDIR /opt/app

RUN apk add --no-cache bash openssl openjdk17 maven gradle python3 py3-pip python3-dev git openssh curl jq yq git-lfs curl ca-certificates step-cli unzip go cosign && \
    pip install --ignore-installed --break-system-packages poetry && \
    yarn install --immutable --immutable-cache --check-cache && \
    curl -sL https://github.com/goreleaser/goreleaser/releases/download/v1.18.2/goreleaser_1.18.2_x86_64.apk -o goreleaser.apk && \
    apk add --allow-untrusted goreleaser.apk && \
    curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash && \
    helm plugin install https://github.com/chartmuseum/helm-push && \
    ln -sf /bin/bash /bin/sh

ENV PATH=$PATH:/root/node_modules/.bin:/opt/app/node_modules/.bin
