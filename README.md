# Semantic Release Image

Simple base image maintained by Renovate for versioning.

Semantic Release documentation can be found here: https://semantic-release.gitbook.io/


## Setup

Simple setup example for GitLab CI:

```yaml
semantic-release:
  image: registry.gitlab.com/lmco/robots/semantic-release-image:latest
  script:
    - semantic-release
  artifacts:
    paths:
      - release.env
    reports:
      dotenv: release.env
```

## Verification

Image can be verified with [Cosign](https://docs.sigstore.dev/cosign/overview) and public key can be found in [Releases](https://gitlab.com/lmco/robots/semantic-release-image/-/releases).  Signature will be verified against Rekor.

Example command with yq for readability, and [crane](https://github.com/google/go-containerregistry/blob/main/cmd/crane/doc/crane_digest.md) to capture digest:

```bash
$ export DGST=$(crane digest registry.gitlab.com/lmco/robots/semantic-release-image:latest)
$ export COSIGN_PUB_KEY=$(cat cosign.pub)
$ COSIGN_EXPERIMENTAL=1 cosign verify --key env://COSIGN_PUB_KEY registry.gitlab.com/lmco/robots/semantic-release-image@$DGST | yq eval -P -


Verification for registry.gitlab.com/lmco/robots/semantic-release-image@sha256:bbd771d1dc194ee6bb4001e6823d4a84167601859c6d3eb3be8fe470590db962 --
The following checks were performed on each of these signatures:
  - The cosign claims were validated
  - Existence of the claims in the transparency log was verified offline
  - The signatures were verified against the specified public key
- critical:
    identity:
      docker-reference: registry.gitlab.com/lmco/robots/semantic-release-image
    image:
      docker-manifest-digest: sha256:bbd771d1dc194ee6bb4001e6823d4a84167601859c6d3eb3be8fe470590db962
    type: cosign container image signature
  optional:
    Bundle:
      SignedEntryTimestamp: MEUCIQD+BYY+e6wIA......
      Payload:
        body: eyJhcGlWZXJzaW9uIjoiMC4wLjEiLCJraW5kIjoiaGFzaGVkcmVrb3JkIiwic3BlYyI6eyJkYXRhIjp7Imhhc2giOnsiYWxnb3JpdGhtIjoic2hhMjU2IiwidmFsdWUiOiI0Njk1NjkzODE5MTIwMjUwN2JkMzNhYTRlMTdmYTQ2MzA1MzgxOTgyNWI2MDkzMzA5OGJhMWJlMWM2MjVjN2VmIn19LCJzaWduYXR1cmUiOnsiY29udGVudCI6Ik1FWUNJUURXaVFWbitXTUZ6UGp1TFA4VlJUSmZWNXBBZ2VEamlkZkxSeWhPdFJHQVlnSWhBSmI4SXF4TXZ5clRYRFRWVGVTa0oxclVCcVo3R1MrcHIxaTlNdmMvYWZINiIsInB1YmxpY0tleSI6eyJjb250ZW50IjoiTFMwdExTMUNSVWRKVGlCUVZVSk1TVU1nUzBWWkxTMHRMUzBLVFVacmQwVjNXVWhMYjFwSmVtb3dRMEZSV1VsTGIxcEplbW93UkVGUlk.......
        integratedTime: 1668192603
        logIndex: 6887.....
        logID: c0d23d6ad406973f9559f3ba.......
```

## Attestation 

You can also verify attestations and signatures by validating the tree.

```bash
$ export DGST=$(crane digest registry.gitlab.com/lmco/robots/semantic-release-image:latest)
$ cosign tree registry.gitlab.com/lmco/robots/semantic-release-image@$DGST

📦 Supply Chain Security Related artifacts for an image: registry.gitlab.com/lmco/robots/semantic-release-image@sha256:97d9e2c819c905b13bd849650aeb7bbafb9dcac9ba1e49851e8d82c40a9697cb
└── 💾 Attestations for an image tag: registry.gitlab.com/lmco/robots/semantic-release-image:sha256-97d9e2c819c905b13bd849650aeb7bbafb9dcac9ba1e49851e8d82c40a9697cb.att
   └── 🍒 sha256:b56dcbd3d1ada9c64a2450a3a2cbd3a0b75a15fdec2eedc123d780c2c3a846a2
└── 🔐 Signatures for an image tag: registry.gitlab.com/lmco/robots/semantic-release-image:sha256-97d9e2c819c905b13bd849650aeb7bbafb9dcac9ba1e49851e8d82c40a9697cb.sig
   └── 🍒 sha256:120a8bc7c8025d57559f21a8f8c6681bb744a1689a423768c50949e233cc5b7b

and.....

$ export DGST=$(crane digest registry.gitlab.com/lmco/robots/semantic-release-image:latest)
$ export COSIGN_PUB_KEY=$(cat cosign.pub)
$ cosign verify-attestation --key env://COSIGN_PUB_KEY --type cyclonedx registry.gitlab.com/lmco/robots/semantic-release-image@$DGST


or...

$ export DGST=$(crane digest registry.gitlab.com/lmco/robots/semantic-release-image:latest)
$ export COSIGN_PUB_KEY=$(cat cosign.pub)
cosign verify-attestation --key env://COSIGN_PUB_KEY --type cyclonedx registry.gitlab.com/lmco/robots/semantic-release-image@$DGST | jq -r '.payload' | base64 --decode
```